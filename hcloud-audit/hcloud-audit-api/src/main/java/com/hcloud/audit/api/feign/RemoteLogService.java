package com.hcloud.audit.api.feign;

import com.hcloud.audit.api.bean.LoginLogBean;
import com.hcloud.audit.api.bean.OperateLogBean;
import com.hcloud.audit.api.feign.factory.RemoteLogFallbackFactory;
import com.hcloud.common.core.base.HCloudResult;
import com.hcloud.common.core.constants.ServerNameContants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Auther hepangui
 * @Date 2018/11/8
 */
@FeignClient(value = ServerNameContants.AUDIT, fallbackFactory = RemoteLogFallbackFactory.class)
public interface RemoteLogService {
    @PostMapping("/operateLog/save")
    HCloudResult saveLog(@RequestBody OperateLogBean operateLog);

    @PostMapping("/loginLog/save")
    HCloudResult saveLog(@RequestBody LoginLogBean loginLog);
}
