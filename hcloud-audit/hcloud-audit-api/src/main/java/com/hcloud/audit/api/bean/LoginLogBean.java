package com.hcloud.audit.api.bean;

import com.hcloud.common.core.base.BaseBean;
import lombok.Data;

/**
 * @Auther hepangui
 * @Date 2018/11/20
 */
@Data
public class LoginLogBean  extends BaseBean {
    private String username;
    private String osName;
    private String device;
    private String browser;
    private String ipAddr;
    private Integer success;
}
