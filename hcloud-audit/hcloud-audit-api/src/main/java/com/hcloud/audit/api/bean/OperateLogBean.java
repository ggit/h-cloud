package com.hcloud.audit.api.bean;

import com.hcloud.common.core.base.BaseBean;
import lombok.Data;

import java.util.Date;

/**
 * @Auther hepangui
 * @Date 2018/11/20
 */
@Data
public class OperateLogBean extends BaseBean {
    private String username;
    private String operate;
    private Long time;
    private String ip;
    private String params;
    private String uri;
}
