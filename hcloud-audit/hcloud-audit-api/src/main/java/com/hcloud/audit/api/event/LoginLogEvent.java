package com.hcloud.audit.api.event;

import org.springframework.context.ApplicationEvent;

/**
 * 系统操作日志的
 * @Auther hepangui
 * @Date 2018/11/20
 */
public class LoginLogEvent extends ApplicationEvent {
    public LoginLogEvent(Object source) {
        super(source);
    }
}
