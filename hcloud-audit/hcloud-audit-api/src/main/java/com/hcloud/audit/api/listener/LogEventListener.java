package com.hcloud.audit.api.listener;

import com.hcloud.audit.api.bean.LoginLogBean;
import com.hcloud.audit.api.bean.OperateLogBean;
import com.hcloud.audit.api.event.LoginLogEvent;
import com.hcloud.audit.api.event.OperateLogEvent;
import com.hcloud.audit.api.feign.RemoteLogService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @Auther hepangui
 * @Date 2018/11/20
 */
@Slf4j
@AllArgsConstructor
@Component
public class LogEventListener {
    private final RemoteLogService remoteLogService;

    @Async
    @EventListener(OperateLogEvent.class)
    public void listenOperateLog(OperateLogEvent event) {
        Object source = event.getSource();
        remoteLogService.saveLog((OperateLogBean) source);
    }

    @Async
    @EventListener(LoginLogEvent.class)
    public void listenLoginLog(OperateLogEvent event) {
        Object source = event.getSource();
        remoteLogService.saveLog((OperateLogBean) source);
    }
}
