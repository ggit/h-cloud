package com.hcloud.audit.api.annontion;

/**
 * @Auther hepangui
 * @Date 2018/11/20
 */
public @interface OperateLog {
    String value();
}
