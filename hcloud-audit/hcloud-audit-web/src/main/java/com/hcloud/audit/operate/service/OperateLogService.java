package com.hcloud.audit.operate.service;

import com.hcloud.audit.api.bean.OperateLogBean;
import com.hcloud.audit.operate.entity.OperateLogEntity;
import com.hcloud.audit.operate.repository.OperateLogRepository;
import com.hcloud.common.crud.service.BaseDataService;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
public interface OperateLogService
        extends BaseDataService<OperateLogEntity, OperateLogBean> {

}
