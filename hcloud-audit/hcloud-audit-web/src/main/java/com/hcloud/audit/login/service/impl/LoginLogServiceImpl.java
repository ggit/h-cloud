package com.hcloud.audit.login.service.impl;

import com.hcloud.audit.api.bean.LoginLogBean;
import com.hcloud.audit.login.entity.LoginLogEntity;
import com.hcloud.audit.login.repository.LoginLogRepository;
import com.hcloud.audit.login.service.LoginLogService;
import com.hcloud.common.crud.service.impl.BaseDataServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Service
public class LoginLogServiceImpl extends
        BaseDataServiceImpl<LoginLogEntity, LoginLogRepository, LoginLogBean>
        implements LoginLogService {


}
