package com.hcloud.audit.login.controller;

import com.hcloud.audit.api.bean.LoginLogBean;
import com.hcloud.audit.login.service.LoginLogService;
import com.hcloud.common.core.constants.AuthConstants;
import com.hcloud.common.crud.controller.BaseDataController;
import com.hcloud.common.crud.service.BaseDataService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@RestController
@RequestMapping("/loginLog")
@AllArgsConstructor
public class LoginLogController extends BaseDataController<LoginLogBean, LoginLogBean> {
    private final LoginLogService loginLogService;


    @Override
    public BaseDataService getBaseDataService() {
        return loginLogService;
    }

    @Override
    public String getAuthPrefix() {
        return AuthConstants.AUDIT_LOGIN_PREFIX;
    }


}
