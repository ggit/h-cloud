package com.hcloud.audit.login.service;

import com.hcloud.audit.api.bean.LoginLogBean;
import com.hcloud.audit.login.entity.LoginLogEntity;
import com.hcloud.common.crud.service.BaseDataService;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
public interface LoginLogService
        extends BaseDataService<LoginLogEntity, LoginLogBean> {

}
