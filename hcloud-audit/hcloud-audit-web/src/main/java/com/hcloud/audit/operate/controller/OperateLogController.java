package com.hcloud.audit.operate.controller;

import com.hcloud.audit.api.bean.OperateLogBean;
import com.hcloud.audit.operate.service.OperateLogService;
import com.hcloud.common.core.constants.AuthConstants;
import com.hcloud.common.crud.controller.BaseDataController;
import com.hcloud.common.crud.service.BaseDataService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@RestController
@RequestMapping("/operateLog")
@AllArgsConstructor
public class OperateLogController extends BaseDataController<OperateLogBean, OperateLogBean> {
    private final OperateLogService loginLogService;


    @Override
    public BaseDataService getBaseDataService() {
        return loginLogService;
    }

    @Override
    public String getAuthPrefix() {
        return AuthConstants.AUDIT_LOGIN_PREFIX;
    }


}
