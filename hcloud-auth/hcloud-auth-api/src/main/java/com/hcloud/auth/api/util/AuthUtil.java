package com.hcloud.auth.api.util;

import com.hcloud.auth.api.user.MyUserDetail;
import com.hcloud.system.api.bean.user.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * 方便获取当前用户，当前权限等
 *
 * @Auther hepangui
 * @Date 2018/11/14
 */
public class AuthUtil {
    /**
     * 获取Authentication
     */
    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    /**
     * 获取用户
     */
    public static User getUser(Authentication authentication) {
        Object principal = authentication.getPrincipal();
        System.out.println(principal);
        if (principal instanceof User) {
            return (User)principal;
        }
        return null;
    }

    public static String getClientId() {
        Authentication authentication = getAuthentication();
        if (authentication instanceof OAuth2Authentication) {
            OAuth2Authentication auth2Authentication = (OAuth2Authentication) authentication;
            return auth2Authentication.getOAuth2Request().getClientId();
        }
        return null;
    }

    /**
     * 获取用户
     */
    public static User getUser() {
        Authentication authentication = getAuthentication();
        System.out.println(authentication);
        if (authentication == null) {
            return null;
        }
        return getUser(authentication);
    }

    /**
     * 获取权限信息
     */
    public static Set<String> getAuthority() {
        Authentication authentication = getAuthentication();
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

        Set<String> authoritySet = new HashSet<>();
        for (GrantedAuthority authority : authorities) {
            authoritySet.add(authority.getAuthority());
        }
        return authoritySet;
    }
}
