package com.hcloud.auth.api.permission;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Stream;

/**
 * @Auther hepangui
 * @Date 2018/11/14
 */
@Slf4j
@Service("ACS")
public class AuthorityCheckService {

    public boolean has(String authority) {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return false;
        }
        var principal = authentication.getPrincipal();
//        if(principal instanceof User){
//            String account = ((User) principal).getAccount();
//            if("hepg".equals(account)){
//                return true;
//            }
//        }
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        System.out.println(authority);
        var a = authorities.stream().takeWhile(grantedAuthority
                -> !grantedAuthority.getAuthority().equals(authority)).count();
        return a != authorities.size();
//
//        for (GrantedAuthority grantedAuthority : authorities) {
//            String authority1 = grantedAuthority.getAuthority();
//            if(authority1!=null && authority1.equals(authority)){
//                return true;
//            }
//        }
//        return false;
    }

    public static void test(String[] args) {
        //java9新特性
        Stream.of("a", "b", "c", "", "e", "f").dropWhile(s -> !s.isEmpty())
                .forEach(System.out::print);

        Stream.of("a", "b", "c", "", "e", "f").takeWhile(s -> !s.isEmpty())
                .forEach(System.out::print);
    }
}
