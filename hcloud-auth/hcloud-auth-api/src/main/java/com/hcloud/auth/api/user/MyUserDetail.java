package com.hcloud.auth.api.user;

import com.hcloud.common.core.constants.CoreContants;
import com.hcloud.system.api.bean.user.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;

/**
 * 实现oauth2的用户接口，并提供对应的方法
 * @Auther hepangui
 * @Date 2018/11/6
 */

public class MyUserDetail implements UserDetails {

    @Getter
    @Setter
    private User baseUser;

    @Getter
    @Setter
    private Set<SimpleGrantedAuthority> authorities;

    public MyUserDetail (User baseUser){
        this.baseUser = baseUser;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return baseUser.getPassword();
    }

    @Override
    public String getUsername() {
        return baseUser.getAccount();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return CoreContants.UER_NOT_LOCKED.equals(baseUser.getState());
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
