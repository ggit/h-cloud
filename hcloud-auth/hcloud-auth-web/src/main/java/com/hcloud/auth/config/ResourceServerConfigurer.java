

package com.hcloud.auth.config;

import com.hcloud.auth.api.config.AbstractOAuth2ResourceServceConfig;
import com.hcloud.common.core.constants.ResourceConstants;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

/**
 * 继承AbstractOAuth2ResourceServceConfig并重写方法
 * @author hepangui
 * @date 2018/11/02
 */
@EnableResourceServer
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResourceServerConfigurer  extends AbstractOAuth2ResourceServceConfig {
    @Override
    protected String getResourceId() {
        return ResourceConstants.AUTH;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers(
                        "/actuator/**",
                        "/oauth/removeToken",
                        "/oauth/delToken/*",
                        "/oauth/listToken",
                        "/oauth/token",
                        "/mobile/**").permitAll()
                .anyRequest().authenticated()
                .and().csrf().disable();
    }


}
