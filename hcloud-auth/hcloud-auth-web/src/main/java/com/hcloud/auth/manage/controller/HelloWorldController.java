package com.hcloud.auth.manage.controller;

import com.hcloud.common.core.base.HCloudResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther hepangui
 * @Date 2018/11/9
 */
@RestController
public class HelloWorldController {
    @GetMapping("/hello")
    public HCloudResult hello(){
        return new HCloudResult<>("helloworld");
    }
}
