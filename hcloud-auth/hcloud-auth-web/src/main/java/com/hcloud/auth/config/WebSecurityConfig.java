package com.hcloud.auth.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

/**
 * 由于使用了ResourceServerCOnfigurer进行token校验，此处不再进行登录校验
 * 但是由于部分的bug，需要在此处首先初始化AuthenticationManager的bean
 * @Auther hepangui
 * @Date 2018/10/26
 */
//@EnableWebSecurity
    @Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .authorizeRequests()
//                .antMatchers(
//                        "/actuator/**",
//                        "/oauth/removeToken",
//                        "/oauth/delToken/*",
//                        "/oauth/listToken",
//                        "/oauth/token",
//                        "/mobile/**").permitAll()
//                .anyRequest().authenticated()
//                .and().csrf().disable();
//    }

    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}
