package com.hcloud.system.role.controller;

import com.hcloud.common.core.base.HCloudResult;
import com.hcloud.common.core.constants.AuthConstants;
import com.hcloud.common.crud.controller.BaseDataController;
import com.hcloud.common.crud.service.BaseDataService;
import com.hcloud.system.api.bean.role.Role;
import com.hcloud.system.api.bean.role.RoleAuth;
import com.hcloud.system.api.bean.role.RoleQuery;
import com.hcloud.system.role.entity.RoleAuthEntity;
import com.hcloud.system.role.service.RoleService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 角色管理
 *
 * @Auther hepangui
 * @Date 2018/11/5
 */
@RestController
@RequestMapping("/role")
@AllArgsConstructor
public class RoleController extends BaseDataController<Role, RoleQuery> {
    private final RoleService roleService;


    @Override
    public BaseDataService getBaseDataService() {
        return roleService;
    }

    @Override
    public String getAuthPrefix() {
        return AuthConstants.SYSTEM_ROLE_PREFIX;
    }

    @PostMapping("authList")
    public HCloudResult<RoleAuth> authList(String roleId) {
        List<RoleAuthEntity> roleAuthEntityByRoleId = roleService.findRoleAuthEntityByRoleId(roleId);
        return new HCloudResult(roleAuthEntityByRoleId);
    }

    @PostMapping("saveAuth")
    public HCloudResult saveAuth(String roleId, String ids) {
        if (roleId == null || "".equals(roleId)) {
            return new HCloudResult("角色不能为空");
        }
        if (ids != null && !"".equals(ids)) {
            this.roleService.saveAuth(roleId, ids);
            return new HCloudResult();
        } else {
            return new HCloudResult("权限不可为空");
        }
    }

    @PostMapping("findAuthorityByRoleIds")
    public HCloudResult<List<String>> findAuthorityByRoles(String roleIds) {
        if (roleIds == null || "".equals(roleIds)) {
            return new HCloudResult("roleId不能为空");
        }
        return new HCloudResult(this.roleService.findAuthorityByRoleIds(Arrays.asList(roleIds.split(","))));

    }

    @DeleteMapping("delete/{id}")
    @Override
    public HCloudResult deleteOne(@PathVariable String id) {
        this.roleService.deleteOne(id);
        return new HCloudResult();
    }
}
