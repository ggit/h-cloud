package com.hcloud.system.authority.service.impl;

import com.hcloud.system.authority.repository.AuthorityRepository;
import com.hcloud.system.authority.service.AuthorityService;
import com.hcloud.system.api.bean.authority.Authority;
import com.hcloud.system.authority.entity.AuthorityEntity;
import com.hcloud.common.crud.service.impl.BaseDataServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Service
public class AuthorityServiceImpl extends
        BaseDataServiceImpl<AuthorityEntity, AuthorityRepository, Authority>
        implements AuthorityService {
}
