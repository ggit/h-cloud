package com.hcloud.system.user.repository;

import com.hcloud.system.user.entity.UserEntity;
import com.hcloud.common.crud.repository.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Repository
public interface UserRepository extends BaseRepository<UserEntity> {
    UserEntity findByAccount(String account);
}
