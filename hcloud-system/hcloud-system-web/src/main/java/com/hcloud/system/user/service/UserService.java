package com.hcloud.system.user.service;

import com.hcloud.common.crud.service.BaseDataService;
import com.hcloud.system.api.bean.user.User;
import com.hcloud.system.user.entity.UserEntity;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
public interface UserService
        extends BaseDataService<UserEntity, User> {
    User findByAccount(String account);

    void modifyPass(User user, String oldPsw, String newPsw);

    void resetPass(String userId);
}
