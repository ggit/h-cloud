package com.hcloud.system.authority.service;

import com.hcloud.common.crud.service.BaseDataService;
import com.hcloud.system.api.bean.authority.Authority;
import com.hcloud.system.authority.entity.AuthorityEntity;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
public interface AuthorityService
        extends BaseDataService<AuthorityEntity, Authority> {
}
