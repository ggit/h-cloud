package com.hcloud.system.user.controller;

import com.hcloud.common.core.constants.AuthConstants;
import com.hcloud.common.crud.annon.HCloudPreAuthorize;
import com.hcloud.auth.api.util.AuthUtil;
import com.hcloud.common.core.base.HCloudResult;
import com.hcloud.common.crud.controller.BaseDataController;
import com.hcloud.system.api.bean.user.User;
import com.hcloud.system.api.bean.user.UserQuery;
import com.hcloud.system.user.service.UserService;
import lombok.AllArgsConstructor;
import com.hcloud.common.crud.service.BaseDataService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController extends BaseDataController<User, UserQuery> {
    private final UserService userService;


    @Override
    public BaseDataService getBaseDataService() {
        return userService;
    }

    @Override
    public String getAuthPrefix() {
        return AuthConstants.SYSTEM_USER_PREFIX;
    }

    @GetMapping("/getByAccount/{account}")
    public HCloudResult getUserByAccount(@PathVariable String account) {
        return new HCloudResult(userService.findByAccount(account));
    }

    @PostMapping("/modifyPass")
    public HCloudResult modifyPass(String oldPsw,String newPsw){
        User user = AuthUtil.getUser();
        this.userService.modifyPass(user,oldPsw,newPsw);
        return new HCloudResult();
    }

    @PostMapping("/resetPass")
    @PreAuthorize("@ACS.has('sys_user_edit')")
    public HCloudResult resetPass(String userId){
        this.userService.resetPass(userId);
        return new HCloudResult();
    }
}
