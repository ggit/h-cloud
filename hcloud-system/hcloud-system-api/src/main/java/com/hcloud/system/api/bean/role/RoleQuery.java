package com.hcloud.system.api.bean.role;

import lombok.Data;

/**
 * 角色查询条件
 * @Auther hepangui
 * @Date 2018/11/5
 */
@Data
public class RoleQuery extends Role {
    private String nameLike;
}
