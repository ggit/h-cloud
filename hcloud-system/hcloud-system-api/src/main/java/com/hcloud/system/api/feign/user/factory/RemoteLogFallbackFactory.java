package com.hcloud.system.api.feign.user.factory;

import feign.hystrix.FallbackFactory;
import com.hcloud.system.api.feign.user.RemoteUserService;
import com.hcloud.system.api.feign.user.fallback.RemoteUserFallbackImpl;
import org.springframework.stereotype.Component;

/**
 * @Auther hepangui
 * @Date 2018/11/8
 */
@Component
public class RemoteLogFallbackFactory implements FallbackFactory<RemoteUserService> {
    @Override
    public RemoteUserService create(Throwable throwable) {
        return new RemoteUserFallbackImpl(throwable);
    }
}
