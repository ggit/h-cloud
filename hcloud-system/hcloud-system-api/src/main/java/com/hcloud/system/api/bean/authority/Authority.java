package com.hcloud.system.api.bean.authority;

import com.hcloud.common.core.base.BaseTreeBean;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 权限/菜单VO
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Data
public class Authority extends BaseTreeBean {
    @NotBlank(message = "权限/菜单名字不能为空")
    private String name;
    @NotNull(message = "排序号不能为空")
    private Integer orderNum;
    private String menuUrl;
    private String menuHref;
    private String menuIcon;
    private String authority;
    @NotNull(message = "权限/菜单类型不可为空")
    private Integer isMenu;

}
