package com.hcloud.system.api.bean.role;

import lombok.Data;
import com.hcloud.common.core.base.BaseBean;

/**
 * 角色与权限关联
 * @Auther hepangui
 * @Date 2018/11/5
 */
@Data
public class RoleAuth extends BaseBean {
    private String roleId;
    private String authId;
}
