package com.hcloud.system.api.bean.user;

import lombok.Data;

/**
 * 用户模糊查询条件
 * @Auther hepangui
 * @Date 2018/11/6
 */
@Data
public class UserQuery extends User{
    private String nameLike;
}
