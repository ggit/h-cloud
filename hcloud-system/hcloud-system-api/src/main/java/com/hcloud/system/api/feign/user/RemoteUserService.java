package com.hcloud.system.api.feign.user;

import com.hcloud.common.core.base.HCloudResult;
import com.hcloud.common.core.constants.ServerNameContants;
import com.hcloud.system.api.bean.user.User;
import com.hcloud.system.api.feign.user.factory.RemoteLogFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @Auther hepangui
 * @Date 2018/11/8
 */
@FeignClient(value = ServerNameContants.SYSTEM,fallbackFactory = RemoteLogFallbackFactory.class)
public interface RemoteUserService {
    @GetMapping("/user/getByAccount/{account}")
    HCloudResult<User> getUserByAccount(@PathVariable("account") String account);

}
