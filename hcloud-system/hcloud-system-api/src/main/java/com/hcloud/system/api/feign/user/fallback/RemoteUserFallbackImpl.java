package com.hcloud.system.api.feign.user.fallback;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.hcloud.common.core.base.HCloudResult;
import com.hcloud.system.api.bean.user.User;
import com.hcloud.system.api.feign.user.RemoteUserService;

/**
 * @Auther hepangui
 * @Date 2018/11/8
 */
@Slf4j
@AllArgsConstructor
public class RemoteUserFallbackImpl implements RemoteUserService {
    private final Throwable cause;

    @Override
    public HCloudResult<User> getUserByAccount(String account) {
        log.error("feign 根据帐号查询用户信息失败:{}", account, cause);
        return null;
    }
}
