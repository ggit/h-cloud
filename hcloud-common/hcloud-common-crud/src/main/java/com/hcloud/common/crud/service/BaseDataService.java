package com.hcloud.common.crud.service;

import com.hcloud.common.core.base.BaseBean;
import com.hcloud.common.core.exception.IdMissingException;
import com.hcloud.common.core.exception.ServiceException;
import com.hcloud.common.crud.entity.BaseEntity;
import com.hcloud.common.crud.repository.BaseRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public interface BaseDataService
        <Entity extends BaseEntity,
                Bean extends BaseBean> {
    String POSTFIX_GT = "Gt";
    String POSTFIX_LT = "Lt";
    String POSTFIX_LIKEFONT = "LikeBefore";
    String POSTFIX_LIKEBACK = "LikeBack";
    String POSTFIX_LIKE = "Like";
    String POSTFIX_NOTEQUAL = "NotEqual";
    String POSTFIX_ISNULL = "IsNull";
    String POSTFIX_ISNOTNULL = "IsNotNull";
    String POSTFIX_GTEQ = "GtOrEqual";
    String POSTFIX_LTEQ = "LtOrEqual";

    BaseRepository<Entity> getRepository();

    Class<Bean> getBeanClass();

    Class<Entity> getEntityClass();

    /**
     * 添加一个实体，实体内应没有id，返回添加成功后的实体
     *
     * @param entity
     * @return
     */
    default Entity add(Entity entity) {
        entity.setCreateTime(new Date());
        return getRepository().save(entity);
    }

    /**
     * 批量添加一个实体集合，返回添加成功后的集合
     *
     * @param entities
     * @return
     */
    default List<Entity> saveAll(List<Entity> entities) {
        return getRepository().saveAll(entities);
    }

    /**
     * 更新实体，必须保证id字段不为空
     *
     * @param entity
     * @return
     */
    default Entity update(Entity entity) {
        if (entity.getId() == null || "".equals(entity.getId())) {
            throw new IdMissingException();
        }
        entity.setUpdateTime(new Date());
        return getRepository().save(entity);
    }

    /**
     * 根据id获取实体，如果一级缓存中存在，则从以及缓存中取
     *
     * @param id
     * @return
     */
    default Entity get(String id) {
        return getRepository().getOne(id);
    }

    /**
     * 删除一个实体，实体必须有id
     *
     * @param entity
     * @throws ServiceException
     */
    default void delete(Entity entity) {
        getRepository().delete(entity);
    }

    /**
     * 根据id删除实体
     *
     * @param id
     * @throws ServiceException
     */
    default void deleteById(String id) {
        getRepository().deleteById(id);
    }

    default boolean exists(String id) {
        Entity entity = this.get(id);
        if (entity == null) {
            return false;
        }
        return true;
    }

    /**
     * 根据传入的example 进行查询，不分页，且条件都是equals
     *
     * @param example
     * @return
     */
    default List<Entity> find(Example<Entity> example) {
        return getRepository().findAll(example);
    }

    /**
     * 分页查询，但是没有查询条件
     *
     * @param pageable
     * @return
     */
    default Page<Entity> find(Pageable pageable) {
        return getRepository().findAll(pageable);
    }


    default Bean getBean(String id) {
        Entity entity = this.get(id);
        if (entity == null) {
            return null;
        }
        Bean bean = null;
        try {
            bean = getBeanClass().getDeclaredConstructor().newInstance();
            BeanUtils.copyProperties(entity, bean);
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
        }
        return bean;
    }

    default Bean add(Bean bean) {
        try {
            Entity entity = getEntityClass().getDeclaredConstructor().newInstance();
            BeanUtils.copyProperties(bean, entity);
            Entity save = this.add(entity);
            Bean bean1 = getBeanClass().getDeclaredConstructor().newInstance();
            BeanUtils.copyProperties(save, bean);
            return bean1;
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
            throw new ServiceException("保存失败");
        }
    }

    default Bean update(Bean bean) {
        try {
            Entity entity = getEntityClass().getDeclaredConstructor().newInstance();
            BeanUtils.copyProperties(bean, entity);
            Entity save = this.update(entity);
            Bean bean1 = getBeanClass().getDeclaredConstructor().newInstance();
            BeanUtils.copyProperties(save, bean);
            return bean1;
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
            throw new ServiceException("保存失败");
        }
    }

    /**
     * 根据传入的Bean和sort查询对应的entity
     * 不进行分页，查出来的是全部
     *
     * @param bean
     * @param sort
     * @return
     */
    default List<Entity> findByCondition(Bean bean, Sort sort) {
        if (bean == null) {
            return new ArrayList<>();
        }

        List<Entity> entities = getRepository().findAll(getEntitySpecification(bean), sort);
        return entities;
    }

    /**
     * 条件查询，根据bean中定义的字段名进行查询，
     * 具体字段命名方式参见 BaseDataServiceImpl
     *
     * @param bean 实际传入的应该是queryBean  继承自bean
     * @return
     */
    default List<Entity> findByCondition(Bean bean) {
        if (bean == null) {
            return new ArrayList<>();
        }

        List<Entity> entities = getRepository().findAll(getEntitySpecification(bean));
        return entities;
    }

    /**
     * 条件查询并分页
     *
     * @param pageable
     * @param bean
     * @return
     */
    default Page<Entity> findByCondition(Pageable pageable, Bean bean) {
        if (bean == null) {
            return this.find(pageable);
        }
//        List<String> fieldList = this.getFieldList(bean.getClass());
        Page<Entity> all = getRepository().findAll(getEntitySpecification(bean), pageable);
        return all;
    }

    /**
     * 将po对象转换为Vo对象
     *
     * @param entity
     * @return
     */
    default Bean entityToBean(Entity entity) {

        try {
            Bean bean = getBeanClass().getDeclaredConstructor().newInstance();
            BeanUtils.copyProperties(entity, bean);
            return bean;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 此处忽略泛型检查，因为在controller中调用时并不知道entity的类型
     * 实际调用时必须保证传入参数为对应的entity
     *
     * @param entities
     * @return
     */
    default List<Bean> listEntityToBean(List entities) {
        List<Bean> list = new ArrayList<>();
        if (entities != null || entities.size() != 0) {
            for (Object entity : entities) {
                list.add(entityToBean((Entity) entity));
            }
        }
        return list;
    }


    private Specification<Entity> getEntitySpecification(Bean bean) {
        List<String> fieldList = this.getFieldList(bean.getClass());
        return (Specification<Entity>) (root, query, criteriaBuilder)
                -> {
            List<Predicate> predicates = new ArrayList<Predicate>();
            if (fieldList != null && fieldList.size() > 0) {
                for (String fieldName : fieldList) {
                    try {
                        Method m = (Method) bean.getClass().getMethod("get" + getMethodName(fieldName));
                        Object value = m.invoke(bean);
                        if (value == null) {
                            continue;
                        }
                        Predicate predicate = this.convertPredicate(root, criteriaBuilder, fieldName, value);
                        predicates.add(predicate);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            return criteriaBuilder.and(predicates
                    .toArray(new Predicate[]{}));
        };
    }

    private Predicate convertPredicate(Root<Entity> root, CriteriaBuilder criteriaBuilder, String fieldName, Object value) {
        if (fieldName.endsWith(POSTFIX_LIKEFONT)) {
            Path path = root.get(fieldName.replace(POSTFIX_LIKEFONT, ""));
            return criteriaBuilder.like(path, value + "%");
        }
        if (fieldName.endsWith(POSTFIX_LIKEBACK)) {
            Path path = root.get(fieldName.replace(POSTFIX_LIKEBACK, ""));
            return criteriaBuilder.like(path, "%" + value);
        }
        if (fieldName.endsWith(POSTFIX_LIKE)) {
            Path path = root.get(fieldName.replace(POSTFIX_LIKE, ""));
            return criteriaBuilder.like(path, "%" + value + "%");
        }
        if (fieldName.endsWith(POSTFIX_GT)) {
            Path path = root.get(fieldName.replace(POSTFIX_GT, ""));
            return criteriaBuilder.greaterThan(path, (Comparable) value);
        }
        if (fieldName.endsWith(POSTFIX_LT)) {
            Path path = root.get(fieldName.replace(POSTFIX_LT, ""));
            return criteriaBuilder.lessThan(path, (Comparable) value);
        }
        if (fieldName.endsWith(POSTFIX_GTEQ)) {
            Path path = root.get(fieldName.replace(POSTFIX_GTEQ, ""));
            return criteriaBuilder.greaterThanOrEqualTo(path, (Comparable) value);
        }
        if (fieldName.endsWith(POSTFIX_LTEQ)) {
            Path path = root.get(fieldName.replace(POSTFIX_LTEQ, ""));
            return criteriaBuilder.lessThanOrEqualTo(path, (Comparable) value);
        }
        if (fieldName.endsWith(POSTFIX_NOTEQUAL)) {
            Path path = root.get(fieldName.replace(POSTFIX_NOTEQUAL, ""));
            return criteriaBuilder.notEqual(path, value);
        }
        if (fieldName.endsWith(POSTFIX_ISNULL)) {
            Path path = root.get(fieldName.replace(POSTFIX_ISNULL, ""));
            return criteriaBuilder.isNull(path);
        }
        if (fieldName.endsWith(POSTFIX_ISNOTNULL)) {
            Path path = root.get(fieldName.replace(POSTFIX_ISNOTNULL, ""));
            return criteriaBuilder.isNotNull(path);
        }
        Path path = root.get(fieldName);
        return criteriaBuilder.equal(path, value);
    }

    private String getMethodName(String fildeName) throws Exception {
        byte[] items = fildeName.getBytes();
        items[0] = (byte) ((char) items[0] - 'a' + 'A');
        return new String(items);
    }

    private List<String> getFieldList(Class<?> clazz) {
        if (null == clazz) {
            return null;
        }
        List<String> fieldList = new LinkedList<String>();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            /** 过滤静态属性**/
            if (Modifier.isStatic(field.getModifiers())) {
                continue;
            }
            /** 过滤transient 关键字修饰的属性**/
            if (Modifier.isTransient(field.getModifiers())) {
                continue;
            }
            fieldList.add(field.getName());
        }
        /** 处理父类字段**/
        Class<?> superClass = clazz.getSuperclass();
        if (superClass.equals(Object.class)) {
            return fieldList;
        }
        fieldList.addAll(getFieldList(superClass));
        return fieldList;
    }
}