package com.hcloud.common.crud.service.impl;

import com.hcloud.common.core.base.BaseBean;
import com.hcloud.common.core.exception.IdMissingException;
import com.hcloud.common.core.exception.ServiceException;
import com.hcloud.common.crud.entity.BaseEntity;
import com.hcloud.common.crud.repository.BaseRepository;
import com.hcloud.common.crud.service.BaseDataService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
public abstract class BaseDataServiceImpl<Entity extends BaseEntity,
        Repository extends BaseRepository<Entity>,
        Bean extends BaseBean>
        implements BaseDataService<Entity, Bean> {

    private Class<Entity> entityClass;
    private Class<Bean> beanClass;
    @Autowired
    protected Repository baseRepository;

    public BaseDataServiceImpl() {
        super();
        Class<?> c = getClass();
        //获取带有泛型参数的父类
        Type type = c.getGenericSuperclass();
        if (type instanceof ParameterizedType) {
            Type[] parameterizedType = ((ParameterizedType) type).getActualTypeArguments();
            entityClass = (Class<Entity>) parameterizedType[0];
            beanClass = (Class<Bean>) parameterizedType[2];
        }
    }

    @Override
    public Class<Entity> getEntityClass() {
        return entityClass;
    }

    @Override
    public Class<Bean> getBeanClass() {
        return beanClass;
    }

    @Override
    public Repository getRepository() {
        return baseRepository;
    }

}
