package com.hcloud.common.core.base;

import lombok.Data;

import java.util.Date;

/**
 * 基础的VO对象bean
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Data
public class BaseBean {
    protected String id;
    protected Date createTime;
    protected Date updateTime;
}
