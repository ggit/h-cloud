package com.hcloud.common.core.base;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * 基础的treeVO对象，定下parentId和Children字段
 * @Auther hepangui
 * @Date 2018/11/5
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class BaseTreeBean extends BaseBean{

    public static final String ROOT_ID = "-1";
    @NotBlank(message = "父节点不可为空")
    private String parentId;
    private List<BaseTreeBean> children;
}
