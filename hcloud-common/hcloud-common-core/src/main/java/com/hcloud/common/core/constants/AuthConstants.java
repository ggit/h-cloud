package com.hcloud.common.core.constants;

/**
 * 认证相关的常量
 *
 * @Auther hepangui
 * @Date 2018/10/28
 */
public interface AuthConstants {
    /**
     * jwt签名关键词
     */
    String JWT_SIGNKEY = "h-cloud";

    /**
     * 存放到token中的额外信息key值
     */
    String USER = "user", AUTHORITY = "authorities";

    String SYSADMIN = "sysadmin";

    String SYSTEM_PREFIX = "sys_",
            SYSTEM_AUTH_PREFIX = SYSTEM_PREFIX + "auth_",
            SYSTEM_USER_PREFIX = SYSTEM_PREFIX + "user_",
            SYSTEM_ROLE_PREFIX = SYSTEM_PREFIX + "role_";

    String AUDIT_PREFIX = "audit_",
            AUDIT_OPERATE_PREFIX = SYSTEM_PREFIX + "operate_",
            AUDIT_LOGIN_PREFIX = SYSTEM_PREFIX + "login_";

}
