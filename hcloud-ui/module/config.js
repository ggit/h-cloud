layui.define(function (exports) {

    var config = {
        base_server: 'http://127.0.0.1/', // 接口地址，实际项目请换成http形式的地址
        tableName: 'userinfo',  // 存储表名
        autoRender: false,  // 窗口大小改变后是否自动重新渲染表格，解决layui数据表格非响应式的问题，目前实现的还不是很好，暂时关闭该功能
        pageTabs: true,   // 是否开启多标签
        // 获取缓存的token
        getToken: function () {
            var t = layui.data(config.tableName)['token'];
            return t;
        },

        // 缓存token
        putToken: function (token) {
            layui.data(config.tableName, {
                key: 'token',
                value: token
            });
        },
        putUser:function(obj){
            layui.data(config.tableName,{
                key:"user",
                value:obj
            });
        },
        getUser:function(){
            return layui.data(config.tableName)['user'];
        },
        putMenus:function(obj){
            layui.data(config.tableName,{
                key:"menus",
                value:obj
            });
        },
        getMenus:function(){
            return layui.data(config.tableName)['menus'];
        },
        putAuthority:function(obj){
            layui.data(config.tableName,{
                key:"authority",
                value:obj
            });
        },
        getAuthority:function(){
            return layui.data(config.tableName)['authority'];
        },

        // 清除user
        removeUser: function () {
            layui.data(config.tableName, {
                key: 'token',
                remove: true
            });
            layui.data(config.tableName, {
                key: 'menus',
                remove: true
            });
            layui.data(config.tableName, {
                key: 'user',
                remove: true
            });
            layui.data(config.tableName, {
                key: 'authority',
                remove: true
            });
        },
    };
    exports('config', config);
});
