package test;

import java.lang.reflect.Method;

/**
 * 生成密码用
 * @Auther hepangui
 * @Date 2018/11/8
 */
public class JasyptTest {


    public static String password = "h-cloud";

    public static String algorithm = "PBEWithMD5AndDES";

    public static void encrypt(String input) {
        ClassLoader classLoader = JasyptTest.class.getClassLoader();
        try {
            Class<?> loadClass = classLoader.loadClass("org.jasypt.intf.cli.JasyptPBEStringEncryptionCLI");
            Method method = loadClass.getMethod("main", String[].class);
            method.invoke(null, new Object[]{new String[]{
                    "input=" + input, "password=" + password, "algorithm=" + algorithm
            }});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void decypt(String input) {
        ClassLoader classLoader = JasyptTest.class.getClassLoader();
        try {
            Class<?> loadClass = classLoader.loadClass("org.jasypt.intf.cli.JasyptPBEStringDecryptionCLI");
            Method method = loadClass.getMethod("main", String[].class);
            method.invoke(null, new Object[]{new String[]{
                    "input=" + input, "password=" + password, "algorithm=" + algorithm
            }});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
//       decypt("ltJPpR50wT0oIY9kfOe1Iw==");
//        decypt("Al8RPp3CKO80Ah+z7cnoi6VKjF9XbncR");
//        encrypt("MyNewPass4!");
        encrypt("root");
    }
}
