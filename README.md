# h-cloud

#### 项目介绍
h-cloud是一个基于OpneJdk11,Spring Cloud Greenwich.M3,Spring Boot 2.1.0.RELEASE 的学习型微服务开发平台，
其中，Eureka作为注册中心，Spring Cloud Gateway 网关， Spring Cloud Config 配置中心，Spring Oauth统一认证中心。
前端采用经典模块化开发框架LayUI，Nginx 路由转发做前后端分离。注释详细，便于大家共同学习

#### 在线体验
[cloud.hepg.net](http://cloud.hepg.net)  用户名：test 密码：123456 
请不要删除或修改其中的内容，尤其是密码。谢谢。

#### 当前架构
- 基于Spring Boot 2.1.0.RELEASE
- 基于Spring Cloud Greenwich.M3
- 网关 Spring Cloud Gateway
- 注册中心 Eureka
- 认证方式 Spring Security oAuth 
- 持久层采用Spring Data JPA ，自动建表，实体类与数据库高度统一
    并封装基础curd的dao，service，controller以及权限控制
- 使用nginx进行前后端分离
- LayUI，使用q.js单页面路由，减少代码，增强体验

#### 当前功能
- 用户管理
- 权限管理（权限控制到按钮）
- 统一认证登录（目前使用jwt）
- 角色管理

#### 未来功能
- 统一日志记录
- 社交登录功能
- 单点登录接口
- 多租户
- 动态路由
- 文件系统（七牛，阿里）
- 分布式任务调度
- 最终一致性事务
- swagger文档整合
- Spring Boot Admin 服务监控
- pingpoint 链路追踪
- 代码生成器

#### 安装教程

1. 下载项目
2. 导入IDEA，准备好JDK11
3. 安装gradle，build
4. 执行buildJar任务生成jar包
5. 准备数据库等环境
5. 运行
#### 项目截图
   ![图片](doc/img/user.png)
   
#### 写在最后
    如果觉得我的项目帮到了您，麻烦您给点个star！ 
    有问题加qq群找我吧：829471660